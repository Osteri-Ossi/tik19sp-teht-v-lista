<?php
$db = null;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Author" content="Oskar Kallunki">
    <title>Tehtävälista</title>
    <link rel="stylesheet" href="css/style.css" type="text/css"/>
</head>
<body>
    <div id="content">
    <?php
    try {
        /*
        // Koska käytän postgresSQL:ää, minulla on ini file jossa salasana ja käyttäjätunnus piilossa
        $ini = file_get_contents("../../pgsqlsalaisuudet.ini");
        $salat = parse_ini_string($ini);
        $dsn = "pgsql:host=localhost;dbname=todo";
        $user = $salat["user"];
        $passwd = $salat["passwd"];
        */

        $dsn = "mysql:host=localhost;dbname=todo";
        $user = 'root';
        $passwd = '';

        $db = new PDO($dsn, $user, $passwd);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }
    catch (PDOException $pdoex) {
        print "<p>Tietokannan avaus epäonnistui. " . $pdoex->getMessage() . "</p>";
    }
    ?>
</body>
</html>