/* 
create database todo

create table task (
id serial primary key,
description varchar(255) not null,
done boolean default false,
added timestamp default current_timestamp) 
*/

create database todo

create table task (
id int AUTO_INCREMENT primary key,
description varchar(255) not null,
done boolean default false,
added timestamp default current_timestamp)